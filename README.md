## Mid-Biotech-php-test

### Для запуска приложения выполнить следующие команды:
*  ```docker-compose build```
*  ```docker-compose up -d```
*  ```docker-compose exec app composer install```
*  ```docker-compose exec app bin/console doctrine:database:create```
*  ```docker-compose exec app bin/console doctrine:migrations:migrate```

###### После поднятия Docker контейнеров, приложение доступно на [localhost](http://localhost:80)
#### Api методы:
* ```api/generate``` для генерации числа и уникального номера
* ```api/retrieve/{id}``` для получения числа и его id (в формате json)
#### Работа с консольной командой генерации числа и получения данных
* ```docker-compose exec app bin/console app:number --set```  для генерации числа и уникального номера
* ```docker-compose exec app bin/console app:number <id> ```  для получения числа и его id
#### Для запуска ежедневной (~~18:30~~ нужно еще разабраться с тайм зоной) генерации отчета в src/Reports/report.txt использовать:
* ```docker-compose exec app bin/console cron:create```
*  name: ```report```
*  Command: ```app:report-number```
*  Schedule: ```30 18 * * *```
*  Description: ```report```
* ```docker-compose exec app bin/console cron:start```
### Задание:
#### Написать REST API для генерации рандомного числа используя PHP-фреймворк на выбор. 
* Каждой генерации присваивать уникальный id по которому можно получить результат генерации. 
* На выходе 2 публичных API метода: generate() и retrieve(id). 

#### Также сделать хотя бы один из следующих пунктов:

* ~~Добавить доставку приложения через docker-compose;~~

* ~~Добавить авторизацию через basic auth;~~

* ~~Добавить консольную команду, которая бы делала то же самое, что и апи (get/set);~~

* ~~Ежедневно генерировать отчёт (в виде .txt-файла) со списком всех сгенерированных чисел;~~

* Ежедневно отправлять на admin@admin.admin письмо со списком всех сгенерированных чисел (не успел)

