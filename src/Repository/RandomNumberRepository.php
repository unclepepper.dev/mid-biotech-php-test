<?php

namespace App\Repository;

use App\Entity\RandomNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RandomNumber>
 *
 * @method RandomNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method RandomNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method RandomNumber[]    findAll()
 * @method RandomNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RandomNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RandomNumber::class);
    }

    public function save(RandomNumber $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RandomNumber $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return array<int, RandomNumber> Returns an array of RandomNumber objects
     */
    public function findAllNumbers(): array
    {
        return $this->createQueryBuilder('r')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findById(string $id): ?RandomNumber
    {
        return $this->createQueryBuilder('rn')
            ->andWhere('rn.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
