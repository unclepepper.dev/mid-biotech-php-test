<?php

namespace App\Command;

use App\Controller\RetrieveController;
use App\Repository\RandomNumberRepository;
use App\Service\NumberGenerateService;
use App\Service\RetrieveNumberService;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:number',
    description: 'generate and get number',
)]
class NumberCommand extends Command
{
    public function __construct(
        private RandomNumberRepository $repository,
        private RetrieveController $controller,
        string $name = null,
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('id', InputArgument::OPTIONAL, 'enter id numbers')
            ->addOption('set', null, InputOption::VALUE_NONE, 'generate number')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $argId = $input->getArgument('id');
        $optSet = $input->getOption('set');

        if ($argId) {
            try {
                $number = $this->controller->retrieve($argId);
                $output->writeln($number);
                $io->success('Success!');
            } catch (Exception $e) {
                $io->error("Errors ID: $argId  is not exist");
            }
        } else {
            if (!$optSet) {
                $io->warning('You need to add options or id value!  Pass --help to see your options.');
            } else {
                try {
                    $numberService = new NumberGenerateService();
                    $number = $numberService->generate();
                    $this->repository->save($number, true);
                    $io->info(sprintf('You generated number: %s, assigned ID for your number: %s', $number->getNumber(), $number->getId()));
                    $io->success('Your number success generated and added to database');
                } catch (Exception $e) {
                    $io->error('Errors ID: Something went wrong');
                }
            }
        }

        return Command::SUCCESS;
    }
}
