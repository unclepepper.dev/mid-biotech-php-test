<?php

namespace App\Command;

use App\Service\ReportGenerateService;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:report-number',
    description: 'Add a short description for your command',
)]
class ReportNumbersCommand extends Command
{
    public function __construct(
        private ReportGenerateService $reportGenerateService,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $this->reportGenerateService->report();
            $io->success('Report is done!');
        } catch (Exception $e) {
            $io->error('Something went wrong');
        }

        return Command::SUCCESS;
    }
}
