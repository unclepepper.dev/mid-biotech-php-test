<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\RandomNumberRepository;
use App\Service\NumberGenerateService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class GenerateController extends AbstractController
{
    public function __construct(
        private NumberGenerateService $generateService,
        private RandomNumberRepository $repository,
    ) {
    }

    #[Route('api/generate', name: 'api_generate')]
    public function generate(): JsonResponse
    {
        try {
            $number = $this->generateService->generate();
            $this->repository->save($number, true);
            $data = [
                'status' => 200,
                'success' => 'Random number added successfully',
            ];

            return $this->response($data);
        } catch (Exception $e) {
            $data = [
                'status' => 422,
                'errors' => 'Something went wrong',
            ];

            return $this->response($data, 422);
        }
    }

    public function response($data, $status = 200, $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }
}
