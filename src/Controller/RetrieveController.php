<?php

namespace App\Controller;

use App\Service\RetrieveNumberService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RetrieveController extends AbstractController
{
    public function __construct(
        private RetrieveNumberService $retrieveNumberService,
    ) {
    }

    #[Route('api/retrieve/{id}', name: 'api_retrieve_id')]
    public function retrieve($id): JsonResponse
    {
        try {
            $numberGenerate = $this->retrieveNumberService->retrieveDataById($id);
            $data = [
                'id' => $numberGenerate->getId(),
                'number' => $numberGenerate->getNumber(),
            ];

            return $this->response($data);
        } catch (Exception $e) {
            $data = [
                'status' => '422',
                'errors' => "ID: $id  is not exist",
            ];

            return $this->response($data, 422);
        }
    }

        #[Route('api/retrieve', name: 'api_retrieve')]
    public function retrieveAll(): JsonResponse
    {
        try {
            $data = [];
            $randomNumbers = $this->retrieveNumberService->retrieveAllData();
            foreach ($randomNumbers as $randomNumber) {
                $data[$randomNumber->getId()] = $randomNumber->getNumber();
            }

            return $this->response($data);
        } catch (Exception $e) {
            $data = [
                'status' => '422',
                'errors' => 'Empty array',
            ];

            return $this->response($data, 422);
        }
    }

    public function response($data, $status = 200, $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }
}
