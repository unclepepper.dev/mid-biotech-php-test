<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\RandomNumber;

class NumberGenerateService
{
    public function generate(): RandomNumber
    {
        $randomNumber = rand(0, 1000);
        $number = new RandomNumber();
        $number->setNumber($randomNumber);

        return $number;
    }
}
