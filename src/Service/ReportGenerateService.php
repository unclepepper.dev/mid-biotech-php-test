<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\RandomNumber;

class ReportGenerateService
{
    public function __construct(
        private RetrieveNumberService $numberService
    ) {
    }

    private const FILE_NAME = __DIR__.'/../Reports/report.txt';

    public function report(): void
    {
        $fd = fopen(self::FILE_NAME, 'w');
        $this->addDataToReport($fd);
        fclose($fd);
    }

    public function addDataToReport($fd): void
    {
        $arrayNumbers = $this->numberService->retrieveAllData();
        foreach ($arrayNumbers as $line) {
            if (!$line instanceof RandomNumber) {
                continue;
            }
            $data = 'ID: '.$line->getId().', Number: '.$line->getNumber()."\r\n";
            fwrite($fd, $data);
        }
    }
}
