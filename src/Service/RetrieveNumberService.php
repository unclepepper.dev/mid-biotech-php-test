<?php

namespace App\Service;

use App\Entity\RandomNumber;
use App\Repository\RandomNumberRepository;
use Doctrine\ORM\NonUniqueResultException;

class RetrieveNumberService
{
    public function __construct(
        private RandomNumberRepository $repository,
    ) {
    }

    /**
     * @return array<int, RandomNumber>
     */
    public function retrieveAllData(): array
    {
        return $this->repository->findAllNumbers();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function retrieveDataById(string $id): ?RandomNumber
    {
        return $this->repository->findById($id);
    }
}
